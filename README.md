
### Introduction

The Master Pipeline consists of the following stages.

- `sanity`: Checks to ensure out pipeline is in a compliant state
- `build`: Stage that dependencies are installed. From here there are stored in a cache for use by jobs later in the following stages.
- `test`: Tests that can run on the code alone with no services stood up to help out. For example, linting and unit tests.
- `publish`: Artifacts published to Nexus. For Java, the `jar` file. For a Node.js library, publishing to Nexus with [npm](https://www.npmjs.com/)
- `docker`: Building the dockerfile.
- `review`: A review app is spun up for the project in the Kubernetes cluster. This review app will be used by the acceptance stage to be used as the System Under Test (SUT).
- `acceptance`: The accpetance tests of the project are run against the SUT review app.
- `confirmation`: Validating the results from the output of previous stages.
- `deploy staging`: **MANUAL**: Must be manually triggered to run. This will deploy the docker image into staging environment.
- `deploy production`: **MANUAL**: Must be manually triggered to run. This will deploy the docker image into production environment.
- `cleanup`: A dummy stage to be used by projects for any cleanup work.


## How to use the Master Pipeline in your own repo

Each project needs to make a `.gitlab-ci.yml` in the root of their repository. That consists of at least the following definitions.

For **Node.js**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
```

For **Java**

```

include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: JAVA
```
## Types of DEPLOYMENTS

For **EKS DEPLOYMENTS** there are two PAC_EKS_CLUSTER that is **crs** and **app** that distinguish if its a **PCI** or **PROD** deployment. For confidential customer data use `PAC_EKS_CLUSTER: crs` and for not confidential customer data use `PAC_EKS_CLUSTER: app`

For **feature branch deployments** include `PAC_DEPLOY_EKS_BRANCH: "TRUE"` **and** `PAC_EKS_CLUSTER: crs` **or** `PAC_EKS_CLUSTER: app`
```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_EKS_BRANCH: "TRUE"
  PAC_EKS_CLUSTER: crs
```
**or**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_EKS_BRANCH: "TRUE"
  PAC_EKS_CLUSTER: app
```

if deploying to **EKS-PCI** include `PAC_DEPLOY_EKS: "TRUE"` **and** `PAC_EKS_CLUSTER: crs`

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: crs
```
if deploying to **EKS-PROD** include include `PAC_DEPLOY_EKS: "TRUE"` **and** `PAC_EKS_CLUSTER: app`

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: app
```

For **OPENSHIFT DEPLOYMENTS**

To update the project include `PAC_DEPLOY_OSE: "TRUE"` **Double quotes are required**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_OSE: "TRUE"
```

To deploy in both **EKS and OPENSHIFT DEPLOYMENTS**

include `PAC_DEPLOY_OSE: "TRUE"` `PAC_DEPLOY_EKS: "TRUE"` and  `PAC_EKS_CLUSTER: crs` **Double quotes are required**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: 'setup.yml'

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_OSE: "TRUE"
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: crs
```
## Automatic deployments to EKS and OSE

**Deployments by default is a manual setup**

To make automatic deployments to any environment use  `PAC_<ENV>_EKS_DEPLOY: "TRUE"` **or** `PAC_<ENV>_OSE_DEPLOY: "TRUE"`

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: setup.yml

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: crs
  PAC_STG_EKS_DEPLOY: "TRUE"
  PAC_TEST_EKS_DEPLOY: "TRUE"
```

**or**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: setup.yml

variables:
  PAC_PLATFORM: NODEJS
  PAC_DEPLOY_OSE: "TRUE"
  PAC_PROD_OSE_DEPLOY: "TRUE"
  PAC_STG_OSE_DEPLOY: "TRUE"
  PAC_TEST_OSE_DEPLOY: "TRUE"
```

## Using jdk version 11

If deploying a mvn project running **openjdk 11**  

Include `PAC_JAVA_VERSION: java11`

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: setup.yml

variables:
  PAC_PLATFORM: "MAVEN"
  PAC_DEPLOY_OSE: "TRUE"
  PAC_JAVA_VERSION: java11
```

## Akamai Cache

If project needs to purge akamai cache include the following variables for each environment
`PAC_AKAMAI_TEST_CODE: "somecode"`  `PAC_AKAMAI_STG_CODE: "somecode"`  `PAC_AKAMAI_PROD_CODE: "somecode"`
Can add all of the variables at once or one at a time.

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: setup.yml

variables:
  PAC_PLATFORM: "MAVEN"
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: crs
  PAC_JAVA_VERSION: java11
  PAC_AKAMAI_TEST_CODE: "somecode"

```
**or**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: setup.yml

variables:
  PAC_PLATFORM: "MAVEN"
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: crs
  PAC_JAVA_VERSION: java11
  PAC_AKAMAI_TEST_CODE: "somecode, somecode"

```

**or**

```
include:
  - project: 'PAC/RDEVOPS/hilton-pipelines'
    file: setup.yml

variables:
  PAC_PLATFORM: "MAVEN"
  PAC_DEPLOY_EKS: "TRUE"
  PAC_EKS_CLUSTER: crs
  PAC_JAVA_VERSION: java11
  PAC_AKAMAI_TEST_CODE: "somecode, somecode"
  PAC_AKAMAI_STG_CODE: "somecode"
  PAC_AKAMAI_PROD_CODE: "somecode"
```

## Environment Variables

| Name           | Description                                  | Examples                                | Required?          |
| ----           | -----------                                  | -------                                 | ----               |
| PAC_PLATFORM   | A toggle for the type of jobs that will run. | `NODEJS` or `MAVEN`                     | YES                |
| PAC_DOCKER_HUB | The location of the Nexus Docker Hub.        | `dockerhub.hilton.com/hilton`           | NO                 |
| PAC_HTTP_PROXY | A toggle for the type of jobs that will run. | `http://cloudproxy.hhc.hilton.com:3128` | NO                 |
| PAC_DEPLOY_OSE | To deploy projects in Openshift              |`"TRUE"`                                  | YES for Deployments|
| PAC_DEPLOY_EKS | To deploy projects in Kubernetes             | `"TRUE"`                                   | YES for Deployments|
| PAC_EKS_CLUSTER| To deploy project in PCI.                    | `crs`                  | YES when deploying to PCI|
| PAC_TEST_EKS_DEPLOY| To enable auto deployments to test env in EKS | `"TRUE"`                           | NO                  |
| PAC_STG_EKS_DEPLOY| To enable auto deployments to staging env in EKS | `"TRUE"`                         | NO                  |
| PAC_PROD_EKS_DEPLOY| To enable auto deployments to prog env in EKS | `"TRUE"`                           | NO                  |
| PAC_TEST_OSE_DEPLOY| To enable auto deployments to test env in OSE | `"TRUE"`                           | NO                  |
| PAC_STG_OSE_DEPLOY| To enable auto deployments to test env in OSE | `"TRUE"`                            | NO                  |
| PAC_PROD_OSE_DEPLOY| To enable auto deployments to test env in OSE | `"TRUE"`                           | NO                  |
| PAC_DEPLOY_EKS_BRANCH| To do feature branch deployments | `"TRUE"`                                      | NO                  |
| PAC_JAVA_VERSION| if running openjdk version 11 | `java11`                                              | NO                  |
| PAC_AKAMAI_TEST_CODE | if project needs to purge akamai cache in Test env| `code`                       | NO                  |
| PAC_AKAMAI_STG_CODE  | if project needs to purge akamai cache in Stg env | `code`                       | NO                  |
| PAC_AKAMAI_PROD_CODE  | if project needs to purge akamai cache in Prod env| `code`                      | NO                  |
| PAC_NOTIFY_STASH  | if project needs to send pipeline status to bitbucket| `"TRUE"`                     | NO                  |
## Testing Pipeline Changes Using Demo Projects

- Will need to enable **pac-trigger-project job** in **.gitlab-ci.yml** file in **hilton-pipeline repository**

- in **git -c http.sslVerify=false clone https://svcpacbuild:$JIRA_PASSWORD@jira.hilton.com/stash/scm/pac/node-project.git** change it to **git -c http.sslVerify=false clone https://svcpacbuild:$JIRA_PASSWORD@jira.hilton.com/stash/scm/pac/mc-lists.git** to run maven demo project

- cd into the right directory for maven project it is **cd mc-lists** for node its **cd node-project**

- and in the line `curl -k -v -X POST -F token=$CI_JOB_TOKEN -F ref=$CI_COMMIT_REF_NAME https://gitlab.svc-m.hhc.hilton.com/api/v4/projects/14/trigger/pipeline` will need to change the project id from **https://gitlab.svc-m.hhc.hilton.com/api/v4/projects/14/trigger/pipeline** to **https://gitlab.svc-m.hhc.hilton.com/api/v4/projects/11/trigger/pipeline** as that is the project id for maven demo project is in gitlab.

### Pipeline Container Requirements
These are the requirements of the container that the pipeline runs inside. Without these the pipeline will fail.
