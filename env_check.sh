#!/bin/bash

function check_GitLabCI_Yaml(){
    if grep -Fxq "stages:" .gitlab-ci.yml
    then
        echo "FAILURE: .GitLab-CI.yml Has been modified. Stages is present.";
        exit 1;
    fi

    if grep -Fxq "stage:" .gitlab-ci.yml
    then
        echo "FAILURE: .GitLab-CI.yml Has been modified. A new job has been entered.";
        exit 1;
    fi
}

check_GitLabCI_Yaml()
